obj-m += libsyslog.o

all:
	make -C /lib/modules/$(shell uname -r)/build M=$(PWD) modules

install: all uninstall
	$(shell sudo insmod libsyslog.ko)

uninstall:
	@echo $(shell sudo rmmod libsyslog)
	@echo 'uninstalled'
status:
	@echo $(shell lsmod | grep "libsyslog") || 'not found'

clean:
	make -C /lib/modules/$(shell uname -r)/build M=$(PWD) clean
